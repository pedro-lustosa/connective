"use strict";

/* Funções Auxiliares */

export const oNodeToArray = function oNodeToArray( evalued, shallow = true )
  { if( !Array.isArray( evalued ) ) return evalued instanceof Node ? Array.of( evalued ) : Array.from( evalued );
    if( shallow ) return evalued.slice();
    return evalued }

export const oBlockEvent = function( event ) // Bloqueia a propagação de dado evento
  { event.stopPropagation() }

export const oAssert = function( expressions, message = "Assertion failed.", errorType = Error ) // Retorna um erro se uma das expressões passadas retornar um valor falso
  { oAssertType( { string: message } );
    if( errorType != Error && ( !errorType[ "prototype" ] || !( errorType[ "prototype" ] instanceof Error ) ) ) throw new TypeError( "Invalid Error object passed." );
    let expressionsList = Array.oMake( expressions );
    for( let expression of expressionsList ) if( !expression ) throw new errorType( message );
    return true }

export const oAssertConstructor = function( constructorMap, direct = false ) // Retorna um erro se um dos alvos passados não for uma instância do construtor em suas chaves
  { for( let constructorString in constructorMap )
      { let valuesList = Array.oMake( constructorMap[ constructorString ] ),
            _constructor = eval( constructorString );
        for( let value of valuesList )
          { if( direct && value.constructor != _constructor ) throw new TypeError( `Direct constructor of value ${ value } is ${ value.constructor.name }, but it was expected it to be ${ constructorString }.` )
            else if( !( value instanceof _constructor ) ) throw new TypeError( `Value ${ value } is not an instance of ${ constructorString }.` ) } }
    return true }

export const oAssertType = function( typeMap ) // Retorna um erro se o tipo de um dos alvos passados não corresponder com o em suas chaves
  { for( let type in typeMap )
      { let valuesList = Array.oMake( typeMap[ type ] );
        for( let value of valuesList )
          { if( typeof value != type ) throw new TypeError( `Type of value ${ value } is ${ typeof value }, but it was expected it to be ${ type }.` ) } }
    return true }

/* Métodos de Números */

Object.defineProperties( Number.prototype,
  { oSegment: // Retorna arranjo com tamanho igual ao número de segmentos passados, e valores rateados entre o do inset e o do alvo
      { value: function oSegment( segments, inset = 0, round = false )
          { oAssertType( { number: [ segments, inset ] } );
            oAssert( !Number.isNaN( segments ) && !Number.isNaN( inset ), "NaN is not a valid argument for 'oSegment'.", RangeError );
            oAssert( this > inset, "Argument 'inset' cannot be greater or equal to the target of 'oSegment'.", RangeError );
            let segmentedValues = [ inset, this ], innerMultiple = ( this - inset ) / ( segments - 1 );
            if( round ) innerMultiple = Math.round( innerMultiple );
            for( let i = 1; i <= segments - 2; i++ ) segmentedValues.splice( i, 0, inset + innerMultiple * i );
            return segmentedValues },
        enumerable: true } } );

/* Strings */

numbers:
  { Object.defineProperty( String, "oNumbers", { value: [], enumerable: true } );
    for( let i = 48; i < 58; i++ ) String.oNumbers.push( String.fromCharCode( i ) ) }

Object.defineProperties( String.prototype,
  { oCount: // Retorna íntegros com a quantidade discreta dos caracteres passados que o alvo possui
      { value: function oCount( charList )
          { let charArray = Array.oMake( charList ), amount = [];
            for( let char of charArray )
              { let counter = 0;
                for( let i = 0; i < this.length; i++ ) if( this[ i ] == char ) counter++;
                amount.push( counter ) }
            return amount.length == 1 ? amount.pop() : amout },
        enumerable: true },
    oFindFirstChar: // Retorna o primeiro caractere de dado arranjo encontrado no alvo, ou o primeiro que não seja deste dado arranjo
      { value: function oFindFirstChar( charset, exclusive = false )
          { if( !Array.isArray( charset ) ) throw new TypeError( "The charset argument of oFindFirstChar must be an array." );
            if( charset.some( char => typeof char !== "string" ) ) throw new TypeError( "The values of charset array must be all strings." );
            if( typeof exclusive !== "boolean" ) throw new TypeError( "The exclusive argument of oFindFirstChar must be a boolean." );
            let actualChar = this.charAt( 0 ), includeExpression = exclusive ? "charset.includes( actualChar )" : "!charset.includes( actualChar )";
            for( let i = 1; actualChar && eval( includeExpression ); i++ ) actualChar = this.charAt( i );
            return actualChar },
        enumerable: true },
    oFindFirstIndex: // Retorna o número de índice do primeiro caractere de dado arranjo encontrado no alvo, ou do primeiro que não seja deste dado arranjo
      { value: function oFindFirstIndex( charset, exclusive = false )
          { let targetChar = this.oFindFirstChar( charset, exclusive );
            return targetChar ? this.indexOf( targetChar ) : -1 },
        enumerable: true },
    oInsertExcerpt: // Inseri dado excerto em dado ponto do alvo
      { value: function oInsertExcerpt( string, start, reverse = false )
          { if( typeof string !== "string" || typeof start !== "string" ) throw new TypeError( "The arguments 'string' and 'start' of oInsertExcerpt must be strings." );
            if( typeof reverse !== "boolean" ) throw new TypeError( "The argument 'reverse' of oInsertExcerpt must be a boolean." );
            if( !this.includes( start ) ) return false;
            let indexMethod = reverse ? "lastIndexOf" : "indexOf";
            return this.slice( 0, this[ indexMethod ]( start ) + start.length ) + string + this.slice( this[ indexMethod ]( start ) + start.length ) },
        enumerable: true },
    oBringExcerpt: // Retorna o trecho contido no alvo entre demarcadores de início e fim
      { value: function oBringExcerpt( start, end = "", exclusive = false )
          { if( typeof start !== "string" || typeof end !== "string" ) throw  new TypeError( "The arguments 'start' and 'end' of oBringExcerpt must be strings." );
            if( typeof exclusive !== "boolean" ) throw new TypeError( "The exclusive argument of oBringExcerpt must be a boolean." );
            let excerpt = this.slice( this.indexOf( start ) );
            excerpt = excerpt.slice( 0, excerpt.indexOf( end ) || excerpt.length );
            return exclusive ? excerpt.slice( start.length ) : excerpt },
        enumerable: true },
    oRemoveExcerpt: // Remove dado excerto do alvo
      { value: function oRemoveExcerpt( start, end = "", reverse = false )
          { if( typeof start !== "string" || typeof end !== "string" ) throw new TypeError( "The arguments 'start' and 'end' of oRemoveExcerpt must be strings." );
            if( typeof reverse !== "boolean" ) throw new TypeError( "The argument 'reverse' of oRemoveExcerpt must be a boolean." );
            if( !this.includes( start ) || ( end && !this.includes( end ) ) ) return false;
            let indexMethod = reverse ? "lastIndexOf" : "indexOf";
            return this.slice( 0, this[ indexMethod ]( start ) ) + this.slice( this[ indexMethod ]( end ) + end.length ) },
        enumerable: true },
    oReplaceExcerpt: // Substitui dado excerto do alvo por outro
      { value: function oReplaceExcerpt( string, target, reverse = false )
          { if( typeof string !== "string" || typeof target !== "string" ) throw new TypeError( "The arguments 'string' and 'target' of oReplaceExcerpt must be strings." );
            if( typeof reverse !== "boolean" ) throw new TypeError( "The argument 'reverse' of oReplaceExcerpt must be a boolean." );
            if( !this.includes( target ) ) return false;
            let indexMethod = reverse ? "lastIndexOf" : "indexOf";
            return this.slice( 0, this[ indexMethod ]( target ) ) + string + this.slice( this[ indexMethod ]( target ) + target.length ) },
        enumerable: true },
    oInvert: // Retorna dada string com o conteúdo do alvo, se esta o conter, excluído
      { value: function oInvert( string, reverse = false )
          { if( typeof string !== "string" ) throw new TypeError( "The argument string of oInvert must be a string." );
            if( typeof reverse !== "boolean" ) throw new TypeError( "The argument reverse of oInvert must be a boolean." );
            if( !string.includes( this ) ) return "";
            return reverse ?
              string.slice( 0, string.lastIndexOf( this ) ) + string.slice( string.lastIndexOf( this ) + this.length ) :
              string.replace( this, "" ) },
        enumerable: true },
    oRemoveChars: // Remove determinados caracteres do alvo
      { value: function oRemoveChars( charList, count = Infinity, reverse = false )
          { if( typeof count !== "number" ) throw new TypeError( "The argument count of oRemoveChars must be a number." );
            if( count < 0 ) throw new RangeError( "The argument count of oRemoveChars must be greater than or equal 0." );
            if( typeof reverse !== "boolean" ) throw new TypeError( "The argument reverse of oRemoveChars must be a boolean." );
            let charArray = Array.oMake( charList ), newString = this;
            for( let char of charArray )
              { for( let i = 0; i < count; i++ )
                  { if( !newString.includes( char ) ) break;
                    newString = reverse ?
                      newString.slice( 0, newString.lastIndexOf( char ) ) + newString.slice( newString.lastIndexOf( char ) + 1 ) :
                      newString.replace( char, "" ) } }
            return newString },
        enumerable: true },
    oRemoveRepeatedChars: // Remove determinada quantidade de caracteres repetidos em dado alvo
      { value: function oRemoveRepeatedChars( charList, spare = 1, reverse = false )
          { if( typeof spare !== "number" ) throw new TypeError( "The argument spare of oRemoveRepeatedChars must be a number." );
            if( spare < 0 ) throw new RangeError( "The argument spare of oRemoveRepeatedChars must be greater or equal to 0." );
            if( typeof reverse !== "boolean" ) throw new TypeError( "The argument reverse of oRemoveChars must be a boolean." );
            if( spare == 0 ) return oRemoveChars( charList );
            let charArray = Array.oMake( charList ), fullString = this;
            for( let char of charArray )
              { if( !fullString.includes( char ) ) continue;
                let sparedExcerpt = reverse ? fullString.slice( 0, fullString.indexOf( char ) + char.length ) : fullString.slice( fullString.lastIndexOf( char ) ),
                    stringPart = reverse ? sparedExcerpt.oInvert( fullString ) : sparedExcerpt.oInvert( fullString, true );
                while( stringPart.includes( char ) && spare != sparedExcerpt.oCount( char ) )
                  { sparedExcerpt = reverse ?
                      sparedExcerpt + stringPart.slice( 0, stringPart.indexOf( char ) + char.length ) :
                      stringPart.slice( stringPart.lastIndexOf( char ) ) + sparedExcerpt;
                    stringPart = reverse ? sparedExcerpt.oInvert( fullString ) : sparedExcerpt.oInvert( fullString, true ) }
                stringPart = stringPart.oRemoveChars( char );
                fullString = reverse ? sparedExcerpt + stringPart : stringPart + sparedExcerpt }
            return fullString },
        enumerable: true },
    oRemoveAdjacentChars: // Remove de dado caractere outros caracteres lhe adjacentes
      { value: function oRemoveAdjacentChars( charMap, direction = "both" )
          { if( typeof charMap !== "object" ) throw new TypeError( "The argument 'charMap' of oRemoveAdjacentChars must be an object, with the chars from which adjacents ones will be removed as keys, and with the chars to remove as values." );
            if( typeof direction !== "string" ) throw new TypeError( "The argument 'direction' of oRemoveAdjacentChars must be a string." );
            if( ![ "both", "left", "right" ].includes( direction ) ) throw new RangeError( "The value of 'direction', in oRemoveAdjacentChars, must be either 'both', 'left' or 'right'." );
            let newString = this, revalue = false;
            for( let baseChar in charMap )
              { let charList = Array.oMake( charMap[ baseChar ] );
                for( let charToRemove of charList )
                  { if( typeof charToRemove !== "string" ) throw new TypeError( "The array values of 'charMap', in function oRemoveAdjacentChars, must be all strings." );
                    if( direction == "both" || direction == "left" )
                      { let targetString = charToRemove + baseChar;
                        if( newString.includes( targetString ) ) revalue = true;
                        while( newString.includes( targetString ) ) newString = removeChar( newString, targetString ) }
                    if( direction == "both" || direction == "right" )
                      { let targetString = baseChar + charToRemove;
                        if( newString.includes( targetString ) ) revalue = true;
                        while( newString.includes( targetString ) ) newString = removeChar( newString, targetString ) }
                    function removeChar( newString, targetString )
                      { return newString.slice( 0, newString.indexOf( targetString ) ) + newString.oBringExcerpt( targetString ).replace( charToRemove, "" ) } } }
            return revalue ? newString.oRemoveAdjacentChars( charMap, direction ) : newString },
        enumerable: true },
    oRemoveRepeatedAdjacentChars:
      { value: function oRemoveRepeatedAdjacentChars( targetChars )
          { let charList = Array.oMake( targetChars ), charMap = { };
            for( let char of charList ) if( char ) charMap[ char ] = char;
            return this.oRemoveAdjacentChars( charMap ) },
        enumerable: true },
    oFilterNumber: // Retorna o primeiro número ou agrupamento numérico do alvo
      { value: function oFilterNumber( decimalSeparator = ".", overlookedChars = [] )
          { if( typeof decimalSeparator !== "string" ) throw new TypeError( "The argument decimalSeparator of oFilterNumber must be a string." );
            if( !Array.isArray( overlookedChars ) ) throw new TypeError( "The argument overlookedChars of oFilterNumber must be an array." );
            if( overlookedChars.includes( decimalSeparator ) || overlookedChars.some( char => String.oNumbers.includes( char ) ) ) throw new RangeError( "The argument overlookedChars of oFilterNumber must not include the decimalSeparator value, or numeric values." );
            if( overlookedChars.some( char => typeof char !== "string" ) ) throw new TypeError( "The values of overlookedChars array must be all strings." );
            if( !String.oNumbers.some( number => this.includes( number ) ) ) return "";
            let numberSet = String.oNumbers.concat( decimalSeparator || [] ), totalSet = numberSet.concat( overlookedChars ),
                numericExcerpt = this.slice( this.oFindFirstIndex( numberSet ) );
            while( numericExcerpt[0] == decimalSeparator && !String.oNumbers.includes( numericExcerpt[1] ) )
              { numericExcerpt = numericExcerpt.slice( numericExcerpt.slice( 1 ).oFindFirstIndex( numberSet ) + 1 ) }
            if( !numericExcerpt.split("").every( char => numberSet.includes( char ) ) ) numericExcerpt = numericExcerpt.slice( 0, numericExcerpt.oFindFirstIndex( totalSet, true ) );
            for( let char of overlookedChars )
              { while( numericExcerpt.includes( char ) ) numericExcerpt = numericExcerpt.replace( char, "" ) }
            filterAdditionalSeparators:
              { let separatorIndex = numericExcerpt.indexOf( decimalSeparator );
                while( decimalSeparator && separatorIndex != numericExcerpt.lastIndexOf( decimalSeparator ) )
                  { numericExcerpt = numericExcerpt.slice( 0, separatorIndex + 1 ) + numericExcerpt.slice( separatorIndex + 1 ).replace( decimalSeparator, "" ) } }
            numericExcerpt = numericExcerpt.replace( decimalSeparator, "." );
            return Number( numericExcerpt ) },
        enumerable: true } } );

/* Métodos de Arranjos */

Object.defineProperties( Array,
  { oMake: // Converte valores quaisquer em arranjos
      { value: function oMake( target, shallow = true )
          { let array =
              target instanceof Object && !( target instanceof String || target instanceof EventTarget ) && typeof target[ Symbol.iterator ] == "function" ?
                Array.from( target ) : Array.of( target );
            return shallow ? array.slice() : array },
        enumerable: true } } );

Object.defineProperties( Array.prototype,
  { oCycle: // Quando um número não for abarcado pelo índice de um arranjo, decompõe seu valor até que o seja
      { value: function oCycle( number )
          { if( number >= this.length )
              { number = number % this.length }
            else if( number < 0 )
              { while( number < 0 ) number += this.length };
            return this[ number ] },
        enumerable: true } } );

/* Métodos da Janela */

Object.defineProperties( window,
  { oSpreadAction: // Espaceia execução de função em intervalos regulares durante seu período de execução
      { value: function oSpreadAction( action, period, ticks = 2, target )
          { oAssertType( { function: action, number: [ period, ticks ] } );
            let parameters = Array.from( arguments ).slice( 4 ),
                actionTimestamp = period.oSegment( ticks ),
                actionFunction = action.bind( target ),
                actionInterval = window.setInterval( actionFunction, actionTimestamp[1] );
            window.setTimeout( () => window.clearInterval( actionInterval ), period );
            return actionFunction() },
        enumerable: true } } );

/* Métodos de Elementos */

Object.defineProperties( Element.prototype,
  { oBringParentsChain: // Retorna arranjo com os elementos ascendentes do alvo, do mais próximo ao mais distante
      { value: function oBringParentChain( baseElement = document.documentElement )
          { oAssertConstructor( { Element: baseElement } );
            let depth = this.oFindDepth( baseElement ), parentsList = [];
            for( let actualParent = this.parentElement, i = 0; i < depth; actualParent = actualParent.parentElement, i++ ) parentsList.push( actualParent );
            return parentsList },
        enumerable: true },
    oBringParents: // Retorna um arranjo com os elementos do primeiro argumento ascendentes do alvo, opcionalmente delimitados por quantidade e alcance
      { value: function oBringParents( elementsList = this.oBringParentsChain(), count = Infinity, range = Infinity )
          { oAssertType( { number: [ count, range ] } );
            let actualParent = this.parentElement, targetParents = [];
            if( count <= 0 ) return [];
            if( range < 0 ) if( ( range = this.oFindDepth() + range ) < 0 ) return count == 1 ? false : [];
            let arrayList = new oElementsArray( elementsList );
            parentsLoop: while( actualParent && arrayList.length && range )
              { while( arrayList.includes( actualParent ) )
                  { targetParents = targetParents.concat( arrayList.splice( arrayList.findIndex( element => element.isSameNode( actualParent ) ), 1 ) );
                    if( targetParents.length == count ) break parentsLoop }
                actualParent = actualParent.parentElement; range-- }
            return count == 1 ? targetParents.pop() : targetParents },
        enumerable: true },
    oDisableEventType: // Desabilita evento se condição retornar valor verdadeiro
      { value: function oDisableEventType( event, parent = this.parentElement.tagName, condition = true )
          { if( typeof event !== "string" || typeof parent !== "string" ) throw new TypeError( "Arguments 'event' and 'parent' of oDisableEventType must be strings." );
            let target = this.parentElement.closest( parent );
            if( condition ) target.addEventListener( event, oBlockEvent, true ) },
        enumerable: true },
    oEnableEventType: // Habilita evento se condição retornar valor verdadeiro
      { value: function oEnableEventType( event, parent = this.parentElement.tagName, condition = true )
          { if( typeof event !== "string" || typeof parent !== "string" ) throw new TypeError( "Arguments 'event' and 'parent' of oDisableEventType must be strings." );
            let target = this.parentElement.closest( parent );
            if( condition ) target.removeEventListener( event, oBlockEvent, true ) },
        enumerable: true },
    oFindDepth: // Retorna um número correspondente à profundidade do alvo na árvore do DOM
      { value: function oFindDepth( baseElement = document.documentElement, depth = 0 )
          { oAssertConstructor( { Element: baseElement } );
            if( !baseElement.contains( this ) ) return false;
            if( baseElement.isSameNode( this ) ) return depth;
            let elementChildren = baseElement.children;
            for( let child of elementChildren )
              { if( child.contains( this ) ) return this.oFindDepth( child, ++depth ) } },
        enumerable: true },
    oRestrictClass: // Dentro de dado grupo, restringe uma classe de modo que apenas o alvo a tenha
      { value: function oRestrictClass( _class, elementsGroup, checkTarget = false )
          { if( typeof _class !== "string" ) throw new TypeError( "The '_class' argument of oRestrictClass must be a string." );
            if( checkTarget && this.classList.contains( _class ) ) return;
            if( oNodeToArray( elementsGroup ).some( element => !( element instanceof Element ) ) ) throw new TypeError( "The 'elementsGroup' argument of oRestrictClass must have only instances of Element." );
            for( let element of elementsGroup )
              { if( element.classList.contains( _class ) ) element.classList.remove( _class ) }
            this.classList.add( _class ) },
        enumerable: true },
    oTriggerMediaLoading: // Habilita a possibilidade de carregamento de dado recurso midiático
      { value: function oTriggerMediaLoading()
          { switch( this.tagName )
              { case "PICTURE": for( let image of this.children ) checkDataset( image ); break;
                case "SOURCE":
                case "IMG": checkDataset( this ); break;
                case "VIDEO":
                case "AUDIO": checkDataset( this );
                  for( let source of this.children ) checkDataset( source ); break;
                default: throw new RangeError( "The target of oTriggerMediaLoading must be either a <picture>, <source>, <img>, <video> or <audio> element." ) }
            function checkDataset( element )
              { let source, attribute;
                if( ( ( source = element.dataset.src ) && ( attribute = "data-src" ) ) || ( ( source = element.dataset.srcset ) && ( attribute = "data-srcset" ) ) )
                  { element.setAttribute( attribute.replace( "data-", "" ), source );
                    element.removeAttribute( attribute ) } } },
        enumerable: true } } );

/* Métodos de Elementos do HTML */

Object.defineProperties( HTMLElement.prototype,
  { oBringChildren: // Retorna um arranjo com os elementos do primeiro argumento descendentes do alvo, opcionalmente delimitados por quantidade e alcance
      { value: function oBringChildren( elementsList = this.querySelectorAll( "*" ), count = Infinity, range = Infinity )
          { oAssertType( { number: [ count, range ] } );
            let actualChildren = this.children, targetChildren = [];
            if( !actualChildren.length || count <= 0 || range <= 0 ) return [];
            let arrayList = new oElementsArray( elementsList );
            childrenLoop: for( let child of actualChildren )
              { while( arrayList.includes( child ) )
                  { targetChildren = targetChildren.concat( arrayList.splice( arrayList.findIndex( element => element.isSameNode( child ) ), 1 ) );
                    if( targetChildren.length == count ) break childrenLoop }
                if( range > 1 && arrayList.length ) targetChildren = targetChildren.concat( child.oBringChildren( arrayList, count - targetChildren.length, range - 1 ) || [] );
                if( targetChildren.length == count ) break }
            return count == 1 ? targetChildren.pop() : targetChildren },
        enumerable: true },
    oControlAnimation: // Controla o início e fim de animações
      { value: function oControlAnimation()
          { return this.style.animationPlayState = window.getComputedStyle( this ).animationPlayState == "paused" ? "running" : "paused" },
        enumerable: true } } )

/*---- Construtores & Pseudoconstrutores ----*/

// Arranjos de Elementos – Construtor

export const oElementsArray = function( elements )
  { let baseArray = Array.oMake( elements );
    oAssertConstructor( { Element: elements } );
    oElementsArray.setProperties( baseArray );
    return baseArray }

Object.defineProperties( oElementsArray,
  { setProperties:
      { value: function setProperties( baseArray )
          { let targetMethods = Object.keys( oElementsArray.prototype );
            for( let method of targetMethods ) baseArray[ method ] = oElementsArray.prototype[ method ];
            return baseArray },
        enumerable: true } } )

// Arranjos de Elementos – Protótipo

Object.defineProperties( oElementsArray.prototype,
  { oSelectForEach: // Itera o alvo e retorna um arranjo com elementos que corresponderem à string de seleção do argumento
      { value: function oSelectForEach( selector, target = "children", count = 1, range = Infinity )
          { oAssertType( { string: selector, number: [ count, range ] } );
            oAssert( count > 0 && range > 0, "The arguments 'count' and 'range' must be greater than 0.", RangeError );
            let selection = [], method;
            switch( target )
              { case "children": method = "oBringChildren"; break;
                case "parents": method = "oBringParentsChain"; break;
                default: throw new RangeError( "The allowed values for argument 'target' are 'children' and 'parents'." ) };
            switch( method )
              { case "oBringChildren": this.forEach( element =>
                  { selection = selection.concat( element.oBringChildren( element.querySelectorAll( selector ), count, range ) || [] ) } ); break;
                case "oBringParentsChain": this.forEach( element =>
                  { let parents = element.oBringParentsChain().slice( 0, range ), matchesCount = 0;
                    for( let parent of parents )
                      { if( parent.matches( selector ) )
                          { selection.push( parent ); matchesCount++;
                            if( matchesCount == count ) break } } } ) }
            return selection },
        enumerable: true } } );
