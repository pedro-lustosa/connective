/* Draggable Slideshow */

export const DraggableSlideshow = function( draggableArea, contentSets, arrowsSet, baseDisplacement )
  { this.draggableArea = draggableArea; draggableArea.baseDisplacement = baseDisplacement;
    draggableArea.displacementList = [ 550, 420 ];
    var contentSets = this.contentSets = Array.from( contentSets ), arrowsSet = this.arrowsSet = Array.from( arrowsSet );
    let middleSet = contentSets.middle = contentSets.find( set => set.classList.contains( "middle" ) ),
        previousArrow = this.arrowsSet.previous = arrowsSet.find( arrow => arrow.classList.contains( "larr" ) ),
        nextArrow = this.arrowsSet.next = arrowsSet.find( arrow => arrow.classList.contains( "rarr" ) );
    draggableArea.addEventListener( "mousedown", event => this.updateReadinessToDrag( event ) );
    draggableArea.addEventListener( "mouseup", event => this.resetDraggableState( event ) );
    draggableArea.addEventListener( "mousemove", event => this.dragSlideArea( event ) )
    previousArrow.addEventListener( "click", () => this.toPrevious() ); nextArrow.addEventListener( "click", () => this.toNext() );
    window.addEventListener( "resize", () => this.evalueDisplacement( this.currentDisplacement ) );
    this.evalueDisplacement( baseDisplacement, true ) }

prototypeKeys:
  { let prototype = DraggableSlideshow.prototype;
    prototype.toPrevious = function()
      { this.updateMiddleSlide( -1 );
        this.draggableArea.prepend( this.draggableArea.lastElementChild ) }
    prototype.toNext = function()
      { this.updateMiddleSlide( +1 );
        this.draggableArea.append( this.draggableArea.firstElementChild ) }
    prototype.updateMiddleSlide = function( count )
      { this.contentSets.middle.classList.add( "side" );
        this.contentSets.middle = this.contentSets.oCycle( this.contentSets.indexOf( this.contentSets.middle ) + count );
        this.contentSets.middle.oRestrictClass( "middle", this.contentSets );
        this.contentSets.middle.classList.remove( "side" ) }
    prototype.updateReadinessToDrag = function( event )
      { this.draggableArea.baseX = event.clientX }
    prototype.resetDraggableState = function()
      { this.draggableArea.baseX = null;
        this.draggableArea.style.transform = `translateX( -${ this.draggableArea.currentDisplacement }px )` }
    prototype.dragSlideArea = function( event )
      { let area = this.draggableArea;
        if( area.baseX == null ) return;
        area.style.transform = `translateX( ${ -area.currentDisplacement + ( event.clientX - area.baseX ) }px )`;
        let alteredDisplacement = Number( area.style.transform.replace( /[^0-9+.-]/g, "" ) );
        if( Math.abs( alteredDisplacement ) >= area.currentDisplacement * 1.3 ) { this.arrowsSet.next.click(); return this.resetDraggableState() }
        if( Math.abs( alteredDisplacement ) <= area.currentDisplacement / 1.3 ) { this.arrowsSet.previous.click(); return this.resetDraggableState() } }
    prototype.evalueDisplacement = function( displacement, firstTime = false )
      { let draggableArea = this.draggableArea,
            baseDisplacement = draggableArea.baseDisplacement,
            list = draggableArea.displacementList;
        draggableArea.currentDisplacement = window.innerWidth > 999 ? list[0] : window.innerWidth > 649 ? list[1] * 1.5 : list[1] * 2;
        if( draggableArea.currentDisplacement != displacement || firstTime ) return this.resetDraggableState() }
        prototype.constructor = DraggableSlideshow }
