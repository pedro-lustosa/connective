/* Double-Sided Element */

export const DoubleSidedElement = function ( container, element, frontFace, backFace )
  { this.element = element; element.container = container; element.frontFace = frontFace; element.backFace = backFace;
    frontFace.addEventListener( "mouseenter", () => this.turnIn() ); backFace.addEventListener( "mouseleave", () => this.turnOut() );
    frontFace.addEventListener( "animationiteration", event => this.stop( event.currentTarget ) ); backFace.addEventListener( "animationiteration", event => this.stop( event.currentTarget ) );
    frontFace.addEventListener( "animationiteration", () => this.hiddenUnturnedFace() ); backFace.addEventListener( "animationiteration", () => this.hiddenUnturnedFace() ) }

prototypeKeys:
  { let prototype = DoubleSidedElement.prototype;
    prototype.turnIn = function()
      { let frontFace = this.element.frontFace, backFace = this.element.backFace;
        frontFace.oDisableEventType( "mouseenter" );
        frontFace.oControlAnimation(); frontFace.turned = false;
        backFace.style.display = "" }
    prototype.stop = function( animatedCard )
      { let frontFace = this.element.frontFace, backFace = this.element.backFace;
        animatedCard.oControlAnimation();
        switch ( true )
          { case ( animatedCard == frontFace && !frontFace.turned ): backFace.oDisableEventType( "mouseleave" ); backFace.oControlAnimation();
                                                                     break;
            case ( animatedCard == backFace && !frontFace.turned ): backFace.oEnableEventType( "mouseleave" );
                                                                    break;
            case ( animatedCard == frontFace && frontFace.turned ): frontFace.oEnableEventType( "mouseenter" );
                                                                    break;
            case ( animatedCard == backFace && frontFace.turned ): frontFace.oControlAnimation() } }
    prototype.turnOut = function()
      { let frontFace = this.element.frontFace, backFace = this.element.backFace;
        backFace.oDisableEventType( "mouseleave" );
        backFace.oControlAnimation(); frontFace.turned = true }
    prototype.hiddenUnturnedFace = function()
      { let frontFace = this.element.frontFace, backFace = this.element.backFace;
        if( frontFace.turned ) { frontFace.style.visibility = ""; backFace.style.display = "none" }
        else frontFace.style.visibility = "hidden" } }
