/* Construtores */

import { DoubleSidedElement } from "../../global/site-presets/constructors/_double-sided-element.js";

/* Identificadores */

export const infoBoxes = document.getElementById( "info-boxes" );
infoBoxes:
  { var boxes = infoBoxes.boxes = Array.from( infoBoxes.getElementsByClassName( "box" ) );
    boxes:
      { var frontFaces = boxes.frontFaces = Array.from( infoBoxes.querySelectorAll( ".box .front-face" ) ),
            backFaces = boxes.backFaces = Array.from( infoBoxes.querySelectorAll( ".box .back-face" ) ) } }

/* Atribuições */

DoubleSidedElement:
  { for( let box of boxes ) new DoubleSidedElement( infoBoxes, box, frontFaces.find( face => box.contains( face ) ), backFaces.find( face => box.contains( face ) ) ) }
