/* Construtores */

import { oElementsArray } from "../../global/libraries/theWheel/theWheel-min.js";
import { DraggableSlideshow } from "../../global/site-presets/constructors/_draggable-slideshow.js";

/* Identificadores */

export const medicationJourney = document.getElementById( "medication-journey" );
medicationJourney:
  { var linksList = medicationJourney.linksList = medicationJourney.querySelector( ".anchors-list" ),
        videosList = medicationJourney.videosList = medicationJourney.querySelector( ".videos-list" ),
        slideshow = medicationJourney.slideshow = medicationJourney.querySelector( ".slideshow" );
    linksList:
      { var linksItems = new oElementsArray( linksList.children ),
            links = linksItems.links = linksItems.oSelectForEach( "a", "children", 1 ) }
    videosList:
      { var videosContainer = videosList.container = videosList.closest( ".back-videos" ),
            videos = videosList.videos = Array.from( videosList.getElementsByTagName( "video" ) ),
            videosExitButton = videos.exitButton = videosContainer.querySelector( ".exit" ) }
    slideshow:
      { var slidesArea = slideshow.area = slideshow.querySelector( ".displayed-content" );
        slidesArea:
          { var slideshowContent = slidesArea.content = slidesArea.getElementsByClassName( "slide-set" );
            slideshowContent:
              { var slideshowImages = slideshowContent.images = slidesArea.querySelectorAll( ".slideshow-image .main-image" ),
                    slideshowTexts = slideshowContent.texts = slidesArea.querySelectorAll( ".slideshow-text" ) } }
        var slideshowArrows = slideshow.arrows = Array.from( slideshow.querySelectorAll( ".arrows-set > *" ) );
        slideshowArrows:
          { var previousArrow = slideshowArrows.previous = slideshowArrows.find( arrow => arrow.classList.contains( "larr" ) ),
                nextArrow = slideshowArrows.next = slideshowArrows.find( arrow => arrow.classList.contains( "rarr" ) ) } } }

/* Atribuições & Eventos */

videoAppearanceControl: // Controle de aparição de vídeos na área do slideshow
  { for( let link of links ) link.addEventListener( "click", () =>
      { if( videosList.displayed && links[ videos.findIndex( video => video.displayed ) ] == link ) return;
        linksList.oDisableEventType( "click" );
        if( videosList.displayed )
          { let displayedVideo = videos.find( video => video.displayed );
            displayedVideo.displayed = false;
            displayedVideo.pause();
            displayedVideo.oControlAnimation() }
        else
          { slidesArea.oControlAnimation();
            for( let element of [ slideshow, videosContainer, slideshowArrows[0].parentElement ] ) element.classList.add( "video-display" ) }
        videosList.displayed = true;
        let targetVideo = videos[ links.indexOf( link ) ];
        targetVideo.displayed = true; targetVideo.oTriggerMediaLoading();
        link.oRestrictClass( "video-display", links, true ) } );
    slidesArea.addEventListener( "animationiteration", event =>
      { if( event.animationName != "fade-out" ) return;
        slidesArea.oControlAnimation();
        if( videosContainer.classList.contains( "video-display" ) )
          { slidesArea.style.visibility = "hidden";
            if( videosList.displayed ) activeVideo( videos.find( video => video.displayed ) ) }
        else linksList.oEnableEventType( "click" ) } );
    for( let video of videos ) video.addEventListener( "animationiteration", function()
      { let targetVideo = videos.find( video => video.displayed );
        if( targetVideo == this ) { targetVideo.oControlAnimation(); targetVideo.load(); linksList.oEnableEventType( "click" ) }
        else { inactiveVideo( this ); if( targetVideo ) activeVideo( targetVideo ) } } )
    videosExitButton.addEventListener( "click", () =>
      { linksList.oDisableEventType( "click" );
        for( let element of [ slideshow, videosContainer, slideshowArrows[0].parentElement ] )
          { if( element != slideshow ) element.style.transitionDelay = "0s";
            element.classList.remove( "video-display" ) };
        videosList.displayed = false;
        let targetVideo = videos.find( video => video.displayed );
        targetVideo.pause(); targetVideo.oControlAnimation(); targetVideo.displayed = false;
        links.find( link => link.classList.contains( "video-display" ) ).classList.remove( "video-display" ) } )
    videosContainer.addEventListener( "transitionend", () =>
      { if( videosContainer.classList.contains( "video-display" ) ) return;
        slidesArea.style.visibility = "visible"; slidesArea.oControlAnimation();
        for( let element of [ videosContainer, slideshowArrows[0].parentElement ] ) element.style.transitionDelay = "" } );
    const activeVideo = function( video )
      { video.style.display = "block"; video.oControlAnimation() }
    const inactiveVideo = function( video )
      { video.oControlAnimation(); video.style.display = "none";
        video.load(); video.pause() } }

linksSwitching:
  { switchLinks:
      { previousArrow.addEventListener( "click", event => switchLinks( -1, event.currentTarget ) );
        nextArrow.addEventListener( "click", event => switchLinks( +1, event.currentTarget ) );
        function switchLinks( count, arrow )
          { let oldLinkItem = linksItems.find( item => item.classList.contains( "current" ) ),
                newLinkItem = linksItems.oCycle( linksItems.indexOf( oldLinkItem ) + count );
            oldLinkItem.className = oldLinkItem.className.replace( /move-.+[^ ]/, "" );
            newLinkItem.classList.add( arrow == previousArrow ? "move-left" : "move-right" );
            newLinkItem.oRestrictClass( "current", linksItems ) } } }

videoSwitching:
  { switchVideos:
      { previousArrow.addEventListener( "click", () =>
          { if( !videosList.displayed ) return;
            links.oCycle( links.indexOf( links[ videos.findIndex( video => video.displayed ) ] ) - 1 ).click() } );
        nextArrow.addEventListener( "click", () =>
          { if( !videosList.displayed ) return;
            links.oCycle( videos.findIndex( video => video.displayed ) + 1 ).click() } ) } }

DraggableSlideshow: // Criação de um slideshow deslizável
  { window.addEventListener( "load", () =>
      new DraggableSlideshow( slidesArea, slideshowContent, slideshowArrows, slideshowContent[0].offsetWidth ) ) }

adjustSlideshowPlacement:
  { for( let _event of [ "load", "resize" ] )
      { window.addEventListener( _event, () =>
          { if( window.innerWidth < 650 ) return slidesArea.style.left = "";
            let baseWidth = window.innerWidth > 999 ? 1920 : 999;
            slidesArea.style.left = ( window.innerWidth - baseWidth ) / 2 + "px" } ) } }
