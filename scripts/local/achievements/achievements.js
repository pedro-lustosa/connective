/* Importações */

import { oElementsArray } from "../../global/libraries/theWheel/theWheel-min.js"

/* Identificadores */

export const achievements = document.getElementById( "achievements" );

achievements:
  { var achievementsList = achievements.list = achievements.querySelector( ".achievements-list" );
    achievementsList:
      { var achievementsItems = achievementsList.itens = new oElementsArray( achievementsList.getElementsByClassName( "achievement" ) );
        achievementsItems:
          { var achievementsTexts = achievementsItems.texts = achievementsItems.oSelectForEach( ".display-text" ) } } }

/* Eventos */

numbersAnimation:
  { let animationInterval = 500, firstTime = true;
    achievementsTexts[0].animationDuration = 600; achievementsTexts[1].animationDuration = 1000;
    achievementsTexts[2].animationDuration = 1400; achievementsTexts[3].animationDuration = 1800;
    window.addEventListener( "load", () => window.setTimeout( setupAnimatedText, animationInterval ) );
    achievementsItems.oCycle(-1).addEventListener( "animationend", resetItems );
    function setupAnimatedText()
      { for( let achievement of achievementsTexts )
          { achievement.count = 0;
            let endNumber = Number( achievement.dataset.value );
            findBaseCount:
              { if( Number.isInteger( endNumber ) && endNumber >= 20 ) achievement.baseCount = 1;
                else if( endNumber >= 20 )
                  { let targetCount = Number( ( endNumber % 1 ).toFixed( 1 ) );
                    while( endNumber % targetCount >= .1 ) targetCount -= .1;
                    achievement.baseCount = targetCount }
                else achievement.baseCount = .1 } }
        for( let achievement of achievementsTexts ) animateText( achievement, achievement.dataset.value ) }
    function animateText( achievement, endNumber )
      { window.oSpreadAction( changeNumber, achievement.animationDuration, endNumber / achievement.baseCount, achievement );
        window.setTimeout( adjustText, achievement.animationDuration, achievement );
        if( firstTime )
          { let achievementItem = achievement.oBringParents( achievementsItems, 1 );
            window.setTimeout( () => achievementItem.classList.add( "lift" ), achievement.animationDuration, achievementItem ) }
        else if( achievementsTexts.oCycle( -1 ) == achievement ) window.setTimeout( restartAnimation, achievement.animationDuration + animationInterval * 10 ) }
    function changeNumber()
      { this.count = Number( ( this.count + this.baseCount ).toFixed( 1 ) );
        this.textContent = this.textContent.replace( /\d+\.*\d*/, this.count ) }
    function adjustText( achievement )
      { if( achievement.dataset.value != achievement.textContent.oFilterNumber() ) achievement.textContent = achievement.textContent.replace( /\d+\.*\d*/, achievement.dataset.value ) }
    function resetItems()
      { for( let item of achievementsItems )
          { item.classList.remove( "lift" ); firstTime = false;
            return window.setTimeout( restartAnimation, animationInterval * 9 ) } }
    function restartAnimation()
      { for( let achievement of achievementsTexts ) resetText( achievement );
        for( let achievement of achievementsTexts ) window.setTimeout( animateText, animationInterval, achievement, Number( achievement.dataset.value ) ) }
    function resetText( achievement )
      { achievement.textContent = achievement.textContent.replace( /\d+\.*\d*/, "0" ); achievement.count = 0 } }
