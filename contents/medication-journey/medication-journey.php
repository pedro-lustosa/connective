<?php require "../../config/functions.php" ?>
<!DOCTYPE html>
<html lang="en">
  <?php get_head( "medication-journey", "Medication Journey", true ) ?>
  <body style="margin: 0">
    <section id="medication-journey">
      <div class="content-container">
        <header class="heading-container">
          <ul class="anchors-list">
            <li class="current move-right"><a>Learn more about medications</a></li>
            <li><a>Provider sees patient</a></li>
            <li><a>Provider enters diagnosis and orders</a></li>
            <li><a>Provider writes prescription</a></li>
            <li><a>Provider office enrolls patient into HUB</a></li>
            <li><a>Patient-specific insurance benefit is checked</a></li>
            <li><a>Benefits results delivered to physician</a></li>
            <li><a>Pharmacy, specialty pharmacy or physician dispense/administer</a></li>
            <li><a>Patient receives medication</a></li>
            <li><a>Patient at home</a></li>
            <li><a>Patient refills and renews medication</a></li>
            <li><a>Patient and providers continue to learn about medication, savings offers, and adherence</a></li>
            <li><a>Patient continues therapy</a></li>
          </ul>
        </header>
        <div class="slideshow">
          <div class="back-videos">
            <ul class="videos-list">
              <li>
                <video preload="none" poster="multimedia/images/medium/m-sample-1.jpg" autoplay controls>
                  <source data-src="multimedia/videos/sample-1.webm" type="video/webm">
                  <source data-src="multimedia/videos/sample-1b.mp4" type="video/mp4">
                </video>
              </li>
              <li>
                <video preload="none" poster="multimedia/images/large/l-sample-1.png" autoplay controls>
                  <source data-src="multimedia/videos/sample-2.webm" type="video/webm">
                  <source data-src="multimedia/videos/sample-2b.mp4" type="video/mp4">
                </video>
              </li>
              <li>
                <video preload="none" poster="multimedia/images/x-large/L-sample-1.png" autoplay controls>
                  <source data-src="multimedia/videos/sample-3.webm" type="video/webm">
                </video>
              </li>
              <li>
                <video preload="none" poster="multimedia/images/x-large/L-sample-2.png" autoplay controls>
                  <source data-src="multimedia/videos/sample-4.webm" type="video/webm">
                </video>
              </li>
              <li>
                <video preload="none" poster="multimedia/images/medium/m-sample-1.jpg" autoplay controls>
                  <source data-src="multimedia/videos/sample-1.webm" type="video/webm">
                  <source data-src="multimedia/videos/sample-1b.mp4" type="video/mp4">
                </video>
              </li>
              <li>
                <video preload="none" poster="multimedia/images/large/l-sample-1.png" autoplay controls>
                  <source data-src="multimedia/videos/sample-2.webm" type="video/webm">
                  <source data-src="multimedia/videos/sample-2b.mp4" type="video/mp4">
                </video>
              </li>
              <li>
                <video preload="none" poster="multimedia/images/x-large/L-sample-1.png" autoplay controls>
                  <source data-src="multimedia/videos/sample-3.webm" type="video/webm">
                </video>
              </li>
              <li>
                <video preload="none" poster="multimedia/images/x-large/L-sample-2.png" autoplay controls>
                  <source data-src="multimedia/videos/sample-4.webm" type="video/webm">
                </video>
              </li>
              <li>
                <video preload="none" poster="multimedia/images/medium/m-sample-1.jpg" autoplay controls>
                  <source data-src="multimedia/videos/sample-1.webm" type="video/webm">
                  <source data-src="multimedia/videos/sample-1b.mp4" type="video/mp4">
                </video>
              </li>
              <li>
                <video preload="none" poster="multimedia/images/large/l-sample-1.png" autoplay controls>
                  <source data-src="multimedia/videos/sample-2.webm" type="video/webm">
                  <source data-src="multimedia/videos/sample-2b.mp4" type="video/mp4">
                </video>
              </li>
              <li>
                <video preload="none" poster="multimedia/images/x-large/L-sample-1.png" autoplay controls>
                  <source data-src="multimedia/videos/sample-3.webm" type="video/webm">
                </video>
              </li>
              <li>
                <video preload="none" poster="multimedia/images/x-large/L-sample-2.png" autoplay controls>
                  <source data-src="multimedia/videos/sample-4.webm" type="video/webm">
                </video>
              </li>
              <li>
                <video preload="none" poster="multimedia/images/medium/m-sample-1.jpg" autoplay controls>
                  <source data-src="multimedia/videos/sample-1.webm" type="video/webm">
                  <source data-src="multimedia/videos/sample-1b.mp4" type="video/mp4">
                </video>
              </li>
            </ul>
            <div class="exit">&times;</div>
          </div>
          <div class="displayed-content">
            <div class="slide-set side">
              <div class="slideshow-image">
                <div class="main-image continues-to-learn"></div>
                <picture class="ilustration">
                  <img src="multimedia/images/x-small/S-ilustration-7.png" alt="" draggable="false">
                </picture>
              </div>
              <div class="slideshow-text">
                <ul class="topics-list">
                  <li>Awareness communications to patients and providers at the point of prescribing and dispensing, as well as out of workflow</li>
                  <li>Via web, SMS, email, portal, and mail</li>
                </ul>
              </div>
            </div>
            <div class="slide-set side">
              <div class="slideshow-image">
                <div class="main-image therapy-continues"></div>
                <picture class="ilustration">
                  <img src="multimedia/images/x-small/S-ilustration-8.png" alt="" draggable="false">
                </picture>
              </div>
              <div class="slideshow-text">
                <ul class="topics-list">
                  <li>Patient adherence communications</li>
                  <li>Patient Refill reminders</li>
                </ul>
              </div>
            </div>
            <div class="slide-set middle">
              <div class="slideshow-image">
                <div class="main-image medications-learn-more"></div>
                <picture class="ilustration">
                  <img src="multimedia/images/x-small/S-ilustration-9.png" alt="" draggable="false">
                </picture>
              </div>
              <div class="slideshow-text">
                <ul class="topics-list">
                  <li>Awareness communications to patients and providers at the point of prescribing and dispensing, as well as out of workflow</li>
                  <li>Via web, SMS, email, portal, and mail</li>
                </ul>
              </div>
            </div>
            <div class="slide-set side">
              <div class="slideshow-image">
                <div class="main-image provider-sees"></div>
                <picture class="ilustration">
                  <img src="multimedia/images/x-small/S-ilustration-10.png" alt="" draggable="false">
                </picture>
              </div>
              <div class="slideshow-text">
                <ul class="topics-list">
                  <li>Patient and provider prescription information</li>
                </ul>
              </div>
            </div>
            <div class="slide-set side">
              <div class="slideshow-image">
                <div class="main-image enters-diagnosis"></div>
                <picture class="ilustration">
                  <img src="multimedia/images/x-small/S-ilustration-11.png" alt="" draggable="false">
                </picture>
              </div>
              <div class="slideshow-text">
                <ul class="topics-list">
                  <li>Clinical alerts and gaps in care communications delivered to providers within EMR workflow</li>
                </ul>
              </div>
            </div>
            <div class="slide-set side">
              <div class="slideshow-image">
                <div class="main-image writes-presciption"></div>
                <picture class="ilustration">
                  <img src="multimedia/images/x-small/S-ilustration-12.png" alt="" draggable="false">
                </picture>
              </div>
              <div class="slideshow-text">
                <ul class="topics-list">
                  <li>Patented communications provide education and cost savings information to patients at the time of prescribing</li>
                  <li>Solutions that improve medication price transparency as EMRs deploy Real Time Benefit Check (RTBC) and electronic prior authorization</li>
                  <li>Key information on branded and specialty medications to the provider’s choice of therapy</li>
                </ul>
              </div>
            </div>
            <div class="slide-set side">
              <div class="slideshow-image">
                <div class="main-image enrolls-patient"></div>
                <picture class="ilustration">
                  <img src="multimedia/images/x-small/S-ilustration-13.png" alt="" draggable="false">
                </picture>
              </div>
              <div class="slideshow-text">
                <ul class="topics-list">
                  <li>Meaningful patient and provider support services available</li>
                </ul>
              </div>
            </div>
            <div class="slide-set side">
              <div class="slideshow-image">
                <div class="main-image insurance-checked"></div>
                <picture class="ilustration">
                  <img src="multimedia/images/x-small/S-ilustration-14.png" alt="" draggable="false">
                </picture>
              </div>
              <div class="slideshow-text">
                <ul class="topics-list">
                  <li>Seamless patient and provider services launched</li>
                  <li>Technology-enabled benefits verification and electronic prior authorization</li>
                  <li>Automated patient support services</li>
                </ul>
              </div>
            </div>
            <div class="slide-set side">
              <div class="slideshow-image">
                <div class="main-image benefits-delivered"></div>
                <picture class="ilustration">
                  <img src="multimedia/images/x-small/S-ilustration-15.png" alt="" draggable="false">
                </picture>
              </div>
              <div class="slideshow-text">
                <ul class="topics-list">
                  <li>Provider portal provides status and results of the benefits investigation process</li>
                </ul>
              </div>
            </div>
            <div class="slide-set side">
              <div class="slideshow-image">
                <div class="main-image pharmacy-administer"></div>
                <picture class="ilustration">
                  <img src="multimedia/images/x-small/S-ilustration-16.png" alt="" draggable="false">
                </picture>
              </div>
              <div class="slideshow-text">
                <ul class="topics-list">
                  <li>Provider in workflow messaging</li>
                  <li>Advanced analytics enabled eligibility checks</li>
                  <li>Patient and provider payment and reimbursement</li>
                </ul>
              </div>
            </div>
            <div class="slide-set side">
              <div class="slideshow-image">
                <div class="main-image receives-medication"></div>
                <picture class="ilustration">
                  <img src="multimedia/images/x-small/S-ilustration-17.png" alt="" draggable="false">
                </picture>
              </div>
              <div class="slideshow-text">
                <ul class="topics-list">
                  <li>Patient savings and trial</li>
                  <li>Patient education</li>
                </ul>
              </div>
            </div>
            <div class="slide-set side">
              <div class="slideshow-image">
                <div class="main-image at-home"></div>
                <picture class="ilustration">
                  <img src="multimedia/images/x-small/S-ilustration-18.png" alt="" draggable="false">
                </picture>
              </div>
              <div class="slideshow-text">
                <ul class="topics-list">
                  <li>Patient engagement via SMS, email, portal and mail</li>
                </ul>
              </div>
            </div>
            <div class="slide-set side">
              <div class="slideshow-image">
                <div class="main-image refills-medication"></div>
                <picture class="ilustration">
                  <img src="multimedia/images/x-small/S-ilustration-19.png" alt="" draggable="false">
                </picture>
              </div>
              <div class="slideshow-text">
                <ul class="topics-list">
                  <li>Digital adherence and refill reminders</li>
                </ul>
              </div>
            </div>
          </div>
          <div class="arrows-set">
            <picture class="larr">
              <img src="multimedia/images/x-small/S-arrow-1.png" alt="Previous" draggable="false">
            </picture>
            <picture class="rarr">
              <img src="multimedia/images/x-small/S-arrow-1.png" alt="Next" draggable="false">
            </picture>
          </div>
        </div>
      </div>
    </section>
  </body>
</html>
