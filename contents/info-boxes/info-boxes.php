<?php require "../../config/functions.php" ?>
<!DOCTYPE html>
<html lang="en-US">
  <?php  get_head( "info-boxes", "Info Boxes", true ) ?>
  <body style="margin: 0">
    <section id="info-boxes">
      <div class="content-container">
        <ul class="boxes-list">
          <li>
            <section class="box">
              <div class="front-face">
                <h3 class="title">Providers</h3>
                <picture class="ilustration">
                  <img src="multimedia/images/small/s-ilustration-10.png" alt="">
                </picture>
              </div>
              <div class="back-face single">
                <h3 class="title">Providers</h3>
                <div class="content-body">
                  <ul class="info-list">
                    <li>
                      <strong class="display-text">1.5+ MN</strong>
                      <span class="side-text">via <abbr>PDR</abbr></span>
                    </li>
                  </ul>
                </div>
              </div>
            </section>
          </li>
          <li>
            <section class="box">
              <div class="front-face">
                <h3 class="title">Prescribers</h3>
                <picture class="ilustration">
                  <img src="multimedia/images/small/s-ilustration-5.png" alt="">
                </picture>
              </div>
              <div class="back-face multi">
                <h3 class="title">Prescribers</h3>
                <div class="content-body">
                  <ul class="info-list">
                    <li>
                      <strong class="display-text">900 K</strong>
                      <span class="side-text">via Email</span>
                    </li>
                    <li>
                      <strong class="display-text">300 K</strong>
                      <span class="side-text">via <abbr>EHR</abbr></span>
                    </li>
                  </ul>
                </div>
              </div>
            </section>
          </li>
          <li>
            <section class="box">
              <div class="front-face">
                <h3 class="title">
                  <abbr class="noun">EHR</abbr><span class="ending">s</span>
                </h3>
                <picture class="ilustration">
                  <img src="multimedia/images/small/s-ilustration-1.png" alt="">
                </picture>
              </div>
              <div class="back-face single">
                <h3 class="title">
                  <abbr class="noun">EHR</abbr><span class="ending">s</span>
                </h3>
                <div class="content-body">
                  <ul class="info-list">
                    <li>
                      <strong class="display-text">300+ K</strong>
                      <span class="side-text">Prescribers</span>
                    </li>
                  </ul>
                </div>
              </div>
            </section>
          </li>
          <li>
            <section class="box">
              <div class="front-face">
                <h3 class="title">Patients</h3>
                <picture class="ilustration">
                  <img src="multimedia/images/small/s-ilustration-2.png" alt="">
                </picture>
              </div>
              <div class="back-face multi">
                <h3 class="title">Patients</h3>
                <div class="content-body">
                  <ul class="info-list">
                    <li>
                      <strong class="display-text">150 MN</strong>
                      <span class="side-text">via <abbr>EHR</abbr></span>
                    </li>
                    <li>
                      <strong class="display-text">60 MN</strong>
                      <span class="side-text">via Pharmacy</span>
                    </li>
                  </ul>
                </div>
              </div>
            </section>
          </li>
          <li>
            <section class="box">
              <div class="front-face">
                <h3 class="title">Pharmacists</h3>
                <picture class="ilustration">
                  <img src="multimedia/images/small/s-ilustration-3.png" alt="">
                </picture>
              </div>
              <div class="back-face single">
                <h3 class="title">Pharmacists</h3>
                <div class="content-body">
                  <ul class="info-list">
                    <li>
                      <strong class="display-text">77 K</strong>
                      <span class="side-text">in Pharmacy</span>
                    </li>
                  </ul>
                </div>
              </div>
            </section>
          </li>
          <li>
            <section class="box">
              <div class="front-face">
                <h3 class="title">Pharmacies</h3>
                <picture class="ilustration">
                  <img src="multimedia/images/small/s-ilustration-4.png" alt="">
                </picture>
              </div>
              <div class="back-face multi">
                <h3 class="title">Pharmacies</h3>
                <div class="content-body">
                  <ul class="info-list">
                    <li>
                      <strong class="display-text">60 K</strong>
                      <span class="side-text">Adjudicate copay</span>
                    </li>
                    <li>
                      <strong class="display-text">16 K</strong>
                      <span class="side-text">Communications</span>
                    </li>
                  </ul>
                </div>
              </div>
            </section>
          </li>
          <li>
            <section class="box">
              <div class="front-face">
                <h3 class="title">Brands</h3>
                <picture class="ilustration">
                  <img src="multimedia/images/small/s-ilustration-8.png" alt="">
                </picture>
              </div>
              <div class="back-face multi">
                <h3 class="title">Brands</h3>
                <div class="content-body">
                  <ul class="info-list">
                    <li>
                      <strong class="display-text">570</strong>
                      <span class="side-text">Copay</span>
                    </li>
                    <li>
                      <strong class="display-text">300</strong>
                      <span class="side-text">Communications</span>
                    </li>
                  </ul>
                </div>
              </div>
            </section>
          </li>
          <li>
            <section class="box">
              <div class="front-face">
                <h3 class="title">Manufacturers</h3>
                <picture class="ilustration">
                  <img src="multimedia/images/small/s-ilustration-9.png" alt="">
                </picture>
              </div>
              <div class="back-face single">
                <h3 class="title">Manufacturers</h3>
                <div class="content-body">
                  <ul class="info-list">
                    <li>
                      <strong class="display-text">100+</strong>
                    </li>
                  </ul>
                </div>
              </div>
            </section>
          </li>
          <li>
            <section class="box">
              <div class="front-face">
                <h3 class="title">Partners</h3>
                <picture class="ilustration">
                  <img src="multimedia/images/small/s-ilustration-6.png" alt="">
                </picture>
              </div>
              <div class="back-face multi">
                <h3 class="title" hidden>Partners</h3>
                <div class="content-body">
                  <ul class="info-list">
                    <li>
                      <strong class="text">Payors</strong>
                    </li>
                    <li>
                      <strong class="text">Aggregators</strong>
                    </li>
                    <li>
                      <strong class="text">Clearing networks</strong>
                    </li>
                  </ul>
                </div>
              </div>
            </section>
          </li>
        </ul>
        <header class="heading-container">
          <h2 class="title">
            Simplifying how patients get on and stay on therapy
            to maximize the benefit of medications.
          </h2>
        </header>
      </div>
    </section>
  </body>
</html>
