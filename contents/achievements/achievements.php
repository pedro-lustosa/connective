<?php require "../../config/functions.php" ?>
<!DOCTYPE html>
<html lang="en-US">
  <?php get_head( "achievements", "Achievements" ) ?>
  <body style="margin: 0">
    <section id="achievements" style="background-image: none; background-color: hsla(240, 0%, 90%, 1)">
      <div class="content-container">
        <ul class="achievements-list">
          <li>
            <div class="achievement">
              <picture class="ilustration">
                <img src="multimedia/images/x-small/S-ilustration-1.png" alt="">
              </picture>
              <span class="display-text" data-value="2">~0+ BN</span>
              <span class="side-text">Transactions<br>per year</span>
            </div>
          </li>
          <li>
            <div class="achievement">
              <picture class="ilustration">
                <img src="multimedia/images/x-small/S-ilustration-2.png" alt="">
              </picture>
              <span class="display-text" data-value="1.5">0+ MN</span>
              <span class="side-text">Providers reached<br>via all channels</span>
            </div>
          </li>
          <li>
            <div class="achievement">
              <picture class="ilustration">
                <img src="multimedia/images/x-small/S-ilustration-3.png" alt="">
              </picture>
              <span class="display-text" data-value="77">0 K</span>
              <span class="side-text">Pharmacists in<br>network</span>
            </div>
          </li>
          <li>
            <div class="achievement">
              <picture class="ilustration">
                <img src="multimedia/images/x-small/S-ilustration-4.png" alt="">
              </picture>
              <span class="display-text" data-value="108">0+ MN</span>
              <span class="side-text">Unique claims<br>processed</span>
            </div>
          </li>
        </ul>
      </div>
    </section>
  </body>
</html>
