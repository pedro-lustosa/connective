<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">

  <base href="../../" target="_self">
  <title><?= $title ?></title>

  <link rel="stylesheet" href='<?= "styles/local/${page}/${page}-min.css" ?>'>

  <script src='<?= "scripts/local/${page}/" . "_${page}.js"?>' type="module"></script>
</head>
